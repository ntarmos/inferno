#include <string.h>
#include <string>
#include <vector>

#include <uriparser/UriBase.h>
#include <uriparser/UriDefsAnsi.h>
#include <uriparser/UriDefsConfig.h>
#include <uriparser/UriDefsUnicode.h>
#include <uriparser/Uri.h>
#include <uriparser/UriIp4.h>

#include <libcroco/libcroco.h>

#include "logger.h"
#include "sac-parser.h"

using namespace std;

void CSSParser::property_selector_cb(CRDocHandler *a_this, CRString *a_name, CRTerm *a_expression, gboolean a_is_important) {
	(void)a_this;
	(void)a_name;
	(void)a_expression;
	(void)a_is_important;
	
	if(!strcmp((char *)cr_string_dup2(a_name), "background-image")) {
		// extract url from url('') function
		char *u = (char *)malloc(4096*sizeof(char)); //FIXME: max url size??
		char *c;
		
		// zero buffer
		memset(u, 0, 4096);
		
		// skip the first 4 bytes, and append all the rest to the buffer
		strcat(u, (char *)cr_term_to_string(a_expression) + 4);
		
		// remove trailing right parenthesis
		c = u;
		while(*c++) {
		   if(*c == ')') // strip trailing parenthesis
		      *c = '\0';
		}
		
		// output url parameter
		//urls.insert(string(u));
		Logger::debug(">>> image url = %s", u);
		
		// free mem block
		if(u != NULL)
			free(u);
	}
}

string CSSParser::recompose_url(const string& baseUrl, const string& relativeUrl) {
	UriParserStateA state;

	UriUriA uriBase;
	UriUriA uriRelative;
	UriUriA uriAbsolute;

	char *uriString;
	int charsRequired;
	string retStr;
	string relUrl = relativeUrl;
	size_t pos;

	while ((pos = relUrl.find_first_of('\\')) != string::npos)
		relUrl.at(pos) = '/';

	// construct internal uriparser representation
	state.uri = &uriRelative;
	if(uriParseUriA(&state, relativeUrl.c_str()) != URI_SUCCESS) {
		uriFreeUriMembersA(&uriRelative);
	}

	state.uri = &uriBase;
	if(uriParseUriA(&state, baseUrl.c_str()) != URI_SUCCESS) {
		uriFreeUriMembersA(&uriRelative);
		uriFreeUriMembersA(&uriBase);
	}

	if(uriAddBaseUriA(&uriAbsolute, &uriRelative, &uriBase) != URI_SUCCESS) {
		uriFreeUriMembersA(&uriRelative);
		uriFreeUriMembersA(&uriBase);
	}

	if(uriToStringCharsRequiredA(&uriAbsolute, &charsRequired) != URI_SUCCESS) {
		uriFreeUriMembersA(&uriAbsolute);
	}
	charsRequired++;

	uriString = new char[charsRequired];
	if(uriString == NULL) {
		//Logger::error("new");
	}

	if(uriToStringA(uriString, &uriAbsolute, charsRequired, NULL) != URI_SUCCESS) {
		//Logger::error("uriToStringA");
	}

	uriFreeUriMembersA(&uriAbsolute);
	uriFreeUriMembersA(&uriRelative);
	uriFreeUriMembersA(&uriBase);

	retStr = uriString;
	if(uriString) delete[] uriString;

	return retStr;
}

std::set<std::string> CSSParser::getUrls() {
	return urls;
}

int CSSParser::parse_css_file(string cssUrl, string filePath) {
	CRParser * parser = NULL ;
	CRDocHandler *sac_handler = NULL ;

	baseUrl = cssUrl;

	// initialize libcroco handler
	parser = cr_parser_new_from_file((const guchar *)filePath.c_str(), CR_ASCII);
	if (!parser) {
		return -1;
	}
	
	// create new SAC document handler
	sac_handler = cr_doc_handler_new () ;
	if (!sac_handler) {
		cr_parser_destroy (parser) ;
		return -1;
	}

	// set callback handlers
	sac_handler->property = property_selector_cb;
	
	// connect callback handlers and do the parsing
	cr_parser_set_sac_handler (parser, sac_handler) ;
	cr_parser_parse (parser) ;
	
	// free allocated resources
	cr_parser_destroy (parser) ;
	cr_doc_handler_unref (sac_handler) ;
	
	return 0 ;
}
