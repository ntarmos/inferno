#include <set>
#include <string>

#include <libcroco/libcroco.h>

using namespace std;

class CSSParser {
	private:
		std::string baseUrl;
		std::set<std::string> urls;

		// convenience functions
		static std::string recompose_url(const string& baseUrl, const string& relativeUrl);
		
		// handlers
		static void property_selector_cb(CRDocHandler *a_this, CRString *a_name, CRTerm *a_expression, gboolean a_is_important);
		static void start_selector_cb (CRDocHandler *a_handler, CRSelector *a_selector);
		static void end_selector_cb (CRDocHandler *a_handler, CRSelector *a_selector);
		
	public:
		// constructors
		CSSParser(string url) { baseUrl = url; }
		CSSParser() {}
		
		// convenience functions
		int parse_css_file(std::string , std::string);
		std::set<std::string> getUrls();
};
